#!/usr/bin/env python
# -*- coding: utf-8 -*-
from fastapi.security import OAuth2PasswordBearer 
from web3 import Web3
import uuid
from fastapi import Request
import json
from utils.db_functions import db_check_user, db_post, db_select, db_update, db_post_delete
from hexbytes import HexBytes
from eth_keys import keys
from eth_abi import decode_single, decode_abi
from utils.const import dirname

from typing import Dict
from eth_account.messages import encode_defunct
w3 = Web3(Web3.HTTPProvider('https://rinkeby.infura.io/v3/28268f30bc314cd389cf65588795a4ec'))
import json

async def verify_signed_dict(bundel:Dict):
    print(bundel)
    signature = bundel["sign"]
    bundel.pop("sign")
    string_dict = json.dumps(bundel,separators=(',', ':'))

    password = w3.toHex(w3.sha3(text=string_dict))

    # if the hex string is equal the sha3 hash
    message = encode_defunct(hexstr=password) 
    sig = Web3.toBytes(hexstr=signature)

    vrs = Web3.toInt(sig[-1]), Web3.toHex(sig[:32]), Web3.toHex(sig[32:64])
    user_addr = w3.eth.account.recover_message(message , vrs)

    role = await db_select(tname='users', sel='role', col='address', con='where', val=user_addr.lower())
    print(role) 
    if role["role"] == "admin":
        bundel["sign"] = signature 
        return { "bundel": bundel, "addr" : user_addr }
    else:
        return False

async def verify_signed_message(sig_dict):
 
    text = w3.toHex(w3.sha3(text=sig_dict["text"]))
    hex_text = sig_dict["message"]

    # if the hex string is equal the sha3 hash
    if str(text) == str(hex_text):
        #print(dict)
        message = encode_defunct(hexstr=sig_dict["message"]) 
        sig = Web3.toBytes(hexstr=sig_dict["sign_message"])
        
        vrs = Web3.toInt(sig[-1]), Web3.toHex(sig[:32]), Web3.toHex(sig[32:64])
        userAddr = w3.eth.account.recover_message(message , vrs)
        return userAddr.lower()
    else:
        return False

async def verif_contract_hash(tx_dict):
    #print(dir(w3.eth))
    #print(tx_dict)

    # try verify if transaction hash exists
    try:
        tx = w3.eth.get_transaction_receipt(tx_dict.txhash)
        #print(tx)
    except:
        print("not found")
        tx_dict.info = "tx details not found"
        tx_dict.error = True
        return tx_dict.dict(exclude_unset=True)

    # load abi and contract address
    gnonsis_addr = tx["contractAddress"]  
    with open(dirname + "/abi.json") as f:
        info_json = json.load(f)

    #contract = w3.eth.contract(address=info_json["contract_addr"], abi=info_json["abi"])
    block = w3.eth.get_transaction_by_block(tx["blockNumber"], 0)

    try: 
        tx_dict.bundel_id = int(w3.toText(block["input"]))
        tx_dict.value = block["value"]
        tx_dict.user_addr = block["from"].lower()
        #tx_dict.contract_addr = tx["contractAddress"] 

        # check if user exits
        bundel = await db_select(tname='bundels', sel='bundel', col='id', con='where', val=tx_dict.bundel_id)
        if type(bundel) is dict:
            bundel_json = json.loads(bundel["bundel"])
            if int(tx_dict.value) == int(bundel_json["value"]):
              return tx_dict.dict(exclude_unset=True) 
        else:
            tx_dict.info = "bundel was not found in the Database"
            tx_dict.error = True
            return tx_dict.dict(exclude_unset=True)

    except Exception as error:
        print(error)
        tx_dict.info = "tx id not found"
        tx_dict.error = True
        return tx_dict.dict(exclude_unset=True)






async def verif_address_hash(tx_dict):

    # try verify if transaction hash exists
    try:
        tx = w3.eth.get_transaction(tx_dict.txhash)
    except:
        tx_dict.info = "tx details not found"
        tx_dict.error = True
        return tx_dict.dict(exclude_unset=True)


    try: 
        tx_dict.bundel_id = int(w3.toText(tx["input"]))
        tx_dict.value = tx["value"]
        tx_dict.user_addr = tx["from"].lower()
        tx_dict.recipient_addr = tx["to"] 

        # check if user exits
        bundel = await db_select(tname='bundels', sel='*', col='id', con='where', val=tx_dict.bundel_id)
        if type(bundel) is dict:
            if( int(tx_dict.value) == int(bundel["value"]) and 
               tx_dict.recipient_addr == bundel["creator"] ):

              return tx_dict.dict(exclude_unset=True) 
        else:
            tx_dict.info = "bundel was not found in the Database"
            tx_dict.error = True
            return tx_dict.dict(exclude_unset=True)

    except Exception as error:
        print(error)
        tx_dict.info = "tx id not found"
        tx_dict.error = True
        return tx_dict.dict(exclude_unset=True)






