from fastapi import Depends, HTTPException 
from passlib.context import CryptContext 
from fastapi.security import OAuth2PasswordBearer 
from models.jwt_user import JWTUser
from datetime import datetime, timedelta
from utils.const import (JWT_EXPIRATION_TIME_MINUTES, JWT_AGORITH, JWT_SECRET_KEY) 
from utils.mesages import MS_UNAUTHORIZED
from utils.debug import pprint
from starlette.status import HTTP_401_UNAUTHORIZED
import jwt
import time
from utils.db_functions import db_check_token_user, db_check_jwt_username 
import logging
log = logging.getLogger(__name__)
pwd_context = CryptContext(schemes=["bcrypt"])
oauth_schema = OAuth2PasswordBearer(tokenUrl="/token")
oauth_schema_verify = OAuth2PasswordBearer(tokenUrl="/verify")

def get_hashed_password(password):
    return pwd_context.hash(password)

def verify_password(plain_password, hashed_passowrd):
    try:
        return pwd_context.verify(plain_password, hashed_passowrd)
    except Exception as e:
        return False

# Get's username and checks user password
async def authenticate_user(user:JWTUser):
    potential_users = await db_check_token_user(user)
    is_valid = False
    for db_user in potential_users:
        if verify_password(user.password, db_user["password"]):
            is_valid = True

    if is_valid :
        user.role = potential_users[0]['role']
        return user

    return None

# Create access JWT token
def create_jwt_email_token(user:JWTUser, expiration_minutes):
    expiration = datetime.utcnow() + timedelta(minutes=expiration_minutes)
    print("start datetime ", datetime.utcnow())
    print("time_espiration ", expiration)
    
    jwt_payload = {
        "sub": user.username,
        "exp": expiration
    } 
    jwt_token = jwt.encode(jwt_payload, JWT_SECRET_KEY, algorithm=JWT_AGORITH )
    return  jwt_token

async def check_jwt_email_token(token:str = Depends(oauth_schema_verify) ):
    print(dir(jwt) )
    try:
        payload = jwt.decode(token, JWT_SECRET_KEY, algorithms=JWT_AGORITH )

        username = payload.get("sub")
        expiration = payload.get("exp")

        if time.time() < expiration:
            is_valid = await db_check_jwt_username(username)
            if is_valid:
                return username 
            else:
                return False
        else:
            return "expired"
    except :
        return "expired"

# Create access JWT token
def create_jwt_token(user:JWTUser):
    expiration = datetime.utcnow() + timedelta(minutes=JWT_EXPIRATION_TIME_MINUTES)
    print("time_espiration ", expiration)
    jwt_payload = {
        "sub": user.username,
        "role": user.role,
        "exp": expiration
    }
        
    jwt_token = jwt.encode(jwt_payload, JWT_SECRET_KEY, algorithm=JWT_AGORITH )
    return  jwt_token

def jwt_payload(token:str = Depends(oauth_schema)):
    try:
        payload = jwt.decode(token, JWT_SECRET_KEY, algorithms=JWT_AGORITH )
        return payload
    except Exception as e:
        return "no payload"
    return "no payload"
    
    # Check whether JWT token is correct
async def check_jwt_token(payload:dict = Depends(jwt_payload)):
    try: 
        username = payload.get("sub")
        role = payload.get("role")
        expiration = payload.get("exp")
        
        if time.time() < expiration:
            is_valid = await db_check_jwt_username(username)
            if is_valid:
                return final_checks(role)
    except Exception as e:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED) 
    raise HTTPException(status_code=HTTP_401_UNAUTHORIZED) 


async def current_user(payload:dict = Depends(jwt_payload)):
    if "sub" in payload: 
        username = payload.get("sub")
        return username
    else:
        return None


# Last checking and returning final
def final_checks(role:str):
    if role == "admin":
        return True
    else:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED) 

# pprint("security.py")
# pprint(get_hashed_password("secret"))
