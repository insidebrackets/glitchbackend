from utils.db import execute, fetch
from models.jwt_user import JWTUser
from models.users import Users
from models.bundels import Bundels 
from models.tx import Tx
from utils.helper_functions import upload_image, update_image, delete_image
from utils.debug import pprint, dprint
import inspect
import uuid
import base64
from passlib.context import CryptContext
pwd_context = CryptContext(schemes=["bcrypt"])

async def db_check_token_user(user: JWTUser):
    try:
        result = await db_select(tname='users', sel='*', col='username', con='where', val=user.username, row=False)
        # print(result)
    except Exception as e:
        print("db check user error")

    # pprint(result)

    # if we have not such I user in the database result is none
    if result is None:
        return None
    else:
        return result


async def db_check_jwt_username(username):

    result = await db_select(tname='users', sel='*', col='username', con='where', val=username)
    # if we have not such I user in the database result is none
    if result is None:
        return False
    else:
        return True


async def db_check_user(username, password):
    user = {}
    user["val1"] = username
    user["val2"] = password
    try:
        user_obj = await db_select(tname='users', sel='*', col='username', con='where', val=username)

        result = pwd_context.verify(password, user_obj["password"])

        if result is False:
            return False
        else:
            return True

    except Exception as e:
        print("db check personal error")


# tname = Table, col = column, con = condion (*,and,or)
# sel = value to select
# if row is false it gone fetch all row ( it's will return an array )
# if row is true it's gone fetch just one ro (it's will return a dict)
# async def db_select(tname, sel=None, col=None, con=None, val=None, row=True)::36


async def db_select(tname, sel=None, col=None, con=None, val=None, row=True):
    #pprint(con)

    # if con,col and value are none it's gonna use select 
    # can be used to select an specific colomn
    if None in ( col, con, val): 
        query = "select %s from %s " % (sel, tname)
        values = None
        result = await fetch(query, row, values)
        return result

    # this can been been remove because it's doing 
    # the same thing as the condition above 
    # but before removing it it's need to be checked whitch fucntion it's using it
    if con == "*" and val == None:
        query = "select %s from %s " % (con, tname)
        values = None
        result = await fetch(query, row, values)
        return result

    if con == "where" and type(val) is not dict:
        pprint("where Pause")
        col_var = ':'+col
        query = "select %s from %s where  %s = %s " % (
            sel, tname, col, col_var)
        values = {col: val}

    if con == "and" and type(val) is dict:

        # adds string : to column name
        col_0 = ':'+col[0]
        col_1 = ':'+col[1]

        # query adds to select data from the Database
        query = "select %s from %s where %s = %s and %s = %s" % (
            sel, tname, col[0], col_0, col[1], col_1)
        values = {col[0]: val['val1'], col[1]: val['val2']}
    try:
        result = await fetch(query, row, values)
        #print('select result')
        #pprint(result)
        return result
    except Exception as e:
        print('select query error')
        print(query)


# insert value to the database
async def db_post(obj, user_addr):
    if 'tname' in obj:

        print(obj)
        # Model name
        tname = obj['tname']

        # checks is username alleready exist
        if 'username' in obj:
            user = await db_select(tname=tname, sel='*', col='username', con='where', val=obj["username"])
            if type(user) is dict:
                return 'username'

        # checks is email alleready exist
        if 'email' in obj:
            user = await db_select(tname=tname, sel='*', col='email', con='where', val=obj["email"])
            if type(user) is dict:
                return 'email'

        # foreign key for the product table
        if tname == 'tx':
            user_id = await db_select(tname='users', sel='id', col='address', con='where', val=user_addr)
            print(user_id)
            obj['user_id'] = user_id["id"]
            # obj['uid'] = uuid.uuid4().hex

        if tname == 'bundels':
            print(user_addr)
            user_id = await db_select(tname='users', sel='id', col='address', con='where', val=user_addr.lower())

            obj['user_id'] = user_id["id"]
            if isinstance(user_id, dict) is False:
                return "bundels" #user does not exits

        # uppercase the first letter of tname
        model_name = tname.title()

        if "images" in obj:
            img_arr = await upload_image(obj)
            obj["images"] = img_arr

        # eval turns string to a function
        # this function validates model like User(**obj)
        model_obj = eval(f"{model_name}(**{obj})")

        pprint(model_obj)
        # pprint(model_obj)

        string_array1 = ', '.join(str(x) for x in model_obj.dict().keys())
        string_array2 = ', '.join(':'+str(x) for x in model_obj.dict().keys())

        print(string_array1)
        print(string_array2)

        query = "INSERT INTO %s (%s) values (%s) " % (
            tname, string_array1, string_array2)
        values = model_obj.dict(exclude_unset=True)

        print(values)
        # print(values)
        result = await execute(query, False, values)
        # the Database returns none if the insert was successful
        if result is None:
            return True
    else:
        return False

async def db_post_delete(obj):
    if 'tname' in obj:
        print("/Delete")

        # remove images on the server
        if "images" in obj:
            await delete_image(obj)
            obj.pop("images")
    
        print(obj)
        tname = obj['tname']
        obj.pop("tname")
        
        query = "DELETE FROM %s where id=:id " % ( tname)
        values = obj

        # print(values)
        result = await execute(query, False, values)
        print(result)
        # the Database returns none if the insert was successful
        if result is None:
            return True
    else:
        return False
                

async def db_update(obj, current_user):
    if 'tname' in obj:

        # Model name
        tname = obj['tname']
        
        old_obj = await db_select(tname=tname, sel='*', col='id', con='where', val=obj["id"])

        # checks is username alleready exists
        if "images" in obj:
            old_images = await db_select(tname=tname, sel='images',  con='where', col='id', val=obj["id"]) 
            img_arr = await update_image(obj, old_images)
            obj["images"] = img_arr

        obj.pop("tname")

        #get list of old dict
        keys = list(o for o in obj.keys())
        
        obj_two = {}

        # compare old value (old_obj)  and new values (obj) and returns
        # the values that have been changed by appending it to a new object()
        for key in keys:
            if obj[key] != old_obj[key]:
                obj_two[key] = obj[key]
        
        # remove old data because its not nedded
        if "create_at" in obj_two:
            obj_two.pop("create_at")

        #if ther is no data to update it should return true
        if len(obj_two) == 0 :
            return True
        # add old id to the new obj
        obj_two["id"] = obj["id"]

        # check if the new Username already in the database
        if 'username' in obj_two: 
            user = await db_select(tname=tname, sel='*', col='username', con='where', val=obj["username"])
            if type(user) is dict:
                return 'exists'


        #########################  dynamic query creater #############################
        if (len([i for i in obj_two.keys()]) < 2):
            string_array1 = ' '.join(str(x) for x in obj_two.keys())
            query = "UPDATE %s SET  %s=:%s WHERE id=:id" % (
                tname, string_array1, string_array1)
            print(query)
        else:
            string_array1 = ', '.join(str(x) for x in obj_two.keys())
            string_array2 = ', '.join(':'+str(x) for x in obj_two.keys())
            query = "UPDATE %s SET (%s) = (%s) WHERE id=:id " % (
                tname, string_array1, string_array2)

        values = obj_two
        print(values)
        print(query)
       
        # store values into database
        result = await execute(query, False, values)
        print(result)
        # the Database returns none if the insert was successful
        if result is None:
            return True    

    else:
        return False
