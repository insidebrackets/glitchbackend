import os
from dotenv import dotenv_values

# path to the config file
dirname = os.path.abspath(os.curdir)
#env_path = dirname + "/.env"
#config = dict(dotenv_values(env_path))


############## SECURITY 

JWT_SECRET_KEY = "LKC8G5VpwhDexC9A8Gawz7665fni3QGabWh6Qdcxxn8g22C5jMLKC8G5VpwhDexC9A8Gawz665fn3QGabWh6dcxxn8g22C5jM" 
JWT_AGORITH = "HS256"
JWT_EXPIRATION_TIME_MINUTES = 60 * 24 * 5 # 5 Days   Minuts | Hour | Days
JWT_EXPIRATION_PW_RESET = 10 # 5 minutes 
JWT_EXPIRATION_REGISTRATION = 60 * 24 * 1 # 1 Days 

############## SERVER

SERVER_IP_PORT = "localhost:8000" 

############## DATABASE

DB_HOST = "localhost"
DB_PORT = 5432
DB_USER = "postgres"
DB_PASSWORD = "postgres"
DB_NAME = "antdb"
DB_URL = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

############## TEST DATABASE

TESTING = False 

TEST_DB_HOST = "localhost"
TEST_DB_PORT = 5432
TEST_DB_USER = "test"
TEST_DB_PASSWORD = "test"
TEST_DB_NAME = "test"
TEST_DB_URL = f"postgresql://{TEST_DB_USER}:{TEST_DB_PASSWORD}@{TEST_DB_HOST}:{TEST_DB_PORT}/{TEST_DB_NAME}"

############## REDIS

REDIS_URL = "redis://localhost"

############## TEST REDIS

TEST_REDIS_URL = "redis://localhost"

############## EMAIL

"""
MAIL_USERNAME = config["EMAIL_ADDRESS"],
MAIL_PASSWORD = config["EMAIL_PASS"],
MAIL_FROM = config["EMAIL_ADDRESS"],
MAIL_PORT = 587,
MAIL_SERVER = "smtp.gmail.com",
MAIL_TLS = True,
MAIL_SSL = False,
USE_CREDENTIALS = True
"""
