#!/usr/bin/env python
# -*- coding: utf-8 -*-

import jwt
from fastapi import (BackgroundTasks, UploadFile, 
                    File, Form, Depends, HTTPException, status)
from pydantic import BaseModel, EmailStr
from typing import List
from fastapi_mail import FastMail, MessageSchema,ConnectionConfig
from models.users import Users  
from utils.const import (MAIL_USERNAME, MAIL_PASSWORD, MAIL_FROM, MAIL_PORT, 
                         MAIL_SERVER, MAIL_TLS, MAIL_SSL, USE_CREDENTIALS, SERVER_IP_PORT) 



conf = ConnectionConfig(
    MAIL_USERNAME = MAIL_USERNAME[0], 
    MAIL_PASSWORD = MAIL_PASSWORD[0],
    MAIL_FROM = MAIL_FROM[0],
    MAIL_PORT = MAIL_PORT[0],
    MAIL_SERVER = MAIL_SERVER[0],
    MAIL_TLS = MAIL_TLS[0],
    MAIL_SSL = MAIL_SSL[0],
    USE_CREDENTIALS = USE_CREDENTIALS 
)


async def send_email(email, token):

    email_list = []
    email_list.append(email)
    template = f"""
        <!DOCTYPE html>
        <html>
        <head>
        </head>
        <body>
            <div style=" display: flex; align-items: center; justify-content: center; flex-direction: column;">
                <h3> Account verify </h3>
                <br>
                <p>Thanks for choosing EasyShopas, please 
                click on the link below to verify your account</p> 
                <a style="margin-top:1rem; padding: 1rem; border-radius: 0.5rem; font-size: 1rem; text-decoration: none; background: #0275d8; color: white;"
                 href="http://{SERVER_IP_PORT}/registration/verify?token={token}">
                    Verify your email
                <a>
                <p style="margin-top:1rem;">If you did not register for EasyShopas, 
                please kindly ignore this email and nothing will happen. Thanks<p>
            </div>
        </body>
        </html>
    """

    message = MessageSchema(
        subject="EasyShopas Account Verification Mail",
        recipients=email_list,  # List of recipients, as many as you can pass 
        body=template,
        subtype="html"
        )

    fm = FastMail(conf)
    await fm.send_message(message)  



async def reset_email(email, token):

    email_list = []
    email_list.append(email)
    template = f""" 
        <!DOCTYPE html>
        <html>
        <head>
        </head>
        <body>
            <div style=" display: flex; align-items: center; justify-content: center; flex-direction: column;">
                <h3> Password Reset </h3>
                <br>
                <p>Please click on the link below to Reset your account password</p> 
                <a style="margin-top:1rem; padding: 1rem; border-radius: 0.5rem; font-size: 1rem; text-decoration: none; background: #0275d8; color: white;"
                 href="http://{SERVER_IP_PORT}/reset/token?token={token}">
                    Verify your email
                <a>
                <p style="margin-top:1rem;">If you did not register for EasyShopas, 
                please kindly ignore this email and nothing will happen. Thanks<p>
            </div>
        </body>
        </html>
    """

    message = MessageSchema(
        subject="Password Reset",
        recipients=email_list,  # List of recipients, as many as you can pass 
        body=template,
        subtype="html"
        )

    fm = FastMail(conf)
    await fm.send_message(message) 
    
