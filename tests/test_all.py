from starlette.testclient import TestClient
from run import app
import asyncio
from utils.db import fetch, execute
from utils.security import get_hashed_password
import logging

log = logging.getLogger(__name__)

client = TestClient(app)
loop = asyncio.get_event_loop()

def insert_user(username, password):
    admin = "admin"
    query = """ INSERT INTO users(username, password, role ) values(:username, :password, :role)"""
    hashed_password = get_hashed_password(password)
    values = {"username": username, "password":hashed_password, "role": admin}

    try:
        loop.run_until_complete(execute(query, False, values)) 
    except Exception as e:
        print("insert user error")

def check_user(username, email):
    query = """select * from users where username=:username and email=:email """
    values = {"username": username, "email":email }

    result = loop.run_until_complete(fetch(query, True, values))

    if result is None:
        return False
    return True

def get_auth_header():
    #insert_user("user1","pass1")
    response = client.post("/token", dict(username="user1", password="pass1"))
    jwt_token = response.json()["access_token"]
    
    auth_header = {"Authorization": f"Bearer {jwt_token}"}
    return auth_header

def clear_db():
    query1 = """delete from users;"""
    query2 = """delete from products;"""
    
    loop.run_until_complete(execute(query2, False)) 
    loop.run_until_complete(execute(query1, False)) 
    
    log.info('deleted all table')

def test_token_successfull():
    insert_user("user1","pass1")
    #import pudb; pudb.set_trace()
    log.info(f'insert user to database')
    response = client.post("/token", dict(username="user1", password="pass1"))
    assert response.status_code == 200
    assert "access_token" in response.json()

    #clear_db()

def test_token_unauthorized():
    response = client.post("/token", dict(username="user1", password="pass"))

    assert response.status_code == 401
    
    #clear_db()

def test_post_user():
    auth_header = get_auth_header()
    hashed_password = get_hashed_password('secret')
    user_dict = {
	"tname" : "users",
	"username": "user2",
	"password": hashed_password,
	"email": "mail@de.de",
	"role":"user"
    }

    response = client.post("/v1/post", json=user_dict, headers=auth_header)
    assert response.status_code  == 201
    assert check_user(user_dict["username"],user_dict["email"]) == True


def test_login():
    auth_header = get_auth_header()
    user_dict = {
	"username": "user2",
	"password": "secret",
    }
    response = client.post("/v1/login", json=user_dict, headers=auth_header)

    assert response.json()['is_valid'] == True

def check_product(titel):

    query = """select * from products where titel=:titel"""
    values = {"titel": titel }

    result = loop.run_until_complete(fetch(query, True, values))

    if result is None:
        return False
    return True

def test_post_products():
    auth_header = get_auth_header()
    product_dict = {
        "tname" : "products",
        "titel": "Gebrante nudel mit Gemüse",
        "description": "Hallo ihr lieben heute bekommt ihr user lieblings gericht"
    }
    response = client.post("/v1/post", json=product_dict, headers=auth_header)
    assert response.status_code  == 201
    assert check_product(product_dict["titel"]) == True

    clear_db()


         
