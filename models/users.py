from pydantic import BaseModel
import enum
from fastapi import Query
from typing import List

class Role(str, enum.Enum):
    admin: str = "admin"
    helper: str = "helper"
    personal: str = "user"

class Users(BaseModel):
    username: str = None,
    address: str = Query(..., regex="^0x[a-fA-F0-9]{40}$")
    email: str = Query(None, regex="([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)")
    social: List[str] = None
    images: List[str] = None 
    role: Role = None
    first_name: str = None
    last_name: str = None
    street: str = None
    street_number: int = None
    phone_number: int = None
    zipcode: int = None
    city: str = None
    state: str = None
    land: str = None

class Email(BaseModel):
    email: str = Query(..., regex="([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)")

