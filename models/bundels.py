#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pydantic import BaseModel
from typing import List,Dict

class Bundels(BaseModel):
    id: int
    user_id: int = None
    user_count: int = None
    addr_list: List[str] 
    buy_sell_tx: List[str] = None
    creator: str
    goal: str
    sign:str
    image_addr:str 
    image_id: str 
    image_url: str
    #multi_addr : str 
    open: bool 
    platform : List[str]
    users: str 
    value: str
