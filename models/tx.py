#!/usr/bin/env python
# -*- coding: utf-8 -*- 
from fastapi import Query
from pydantic import BaseModel
from typing import List

class Tx(BaseModel):
    bundel_id: int = None
    txhash: str = Query(..., regex="^0x([A-Fa-f0-9]{64})$")
    value: str = None
    user_addr: str = None
    recipient_addr: str = None
    verified: bool = None
    error: bool = None
    user_id: int = None
    info : str = None
        
